<?php

namespace dto;

enum BlogsOrder: string
{
    case Last = "Сўнги янгиликлар";
    case Trending = "Тренддаги янгиликлар";
    case MostViewed = "Енг кўп кўрилган";
}
