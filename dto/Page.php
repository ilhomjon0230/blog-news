<?php

namespace dto;

class Page
{
    private array $data;
    private bool $firstPage;
    private bool $lastPage;
    private int $currentPage;
    private int $pagesCount;

    /**
     * @param array $data
     * @param bool $firstPage
     * @param bool $lastPage
     * @param int $currentPage
     * @param int $pagesCount
     */
    public function __construct(array $data, bool $firstPage, bool $lastPage, int $currentPage, int $pagesCount)
    {
        $this->data = $data;
        $this->firstPage = $firstPage;
        $this->lastPage = $lastPage;
        $this->currentPage = $currentPage;
        $this->pagesCount = $pagesCount;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return bool
     */
    public function isFirstPage(): bool
    {
        return $this->firstPage;
    }

    /**
     * @param bool $firstPage
     */
    public function setFirstPage(bool $firstPage): void
    {
        $this->firstPage = $firstPage;
    }

    /**
     * @return bool
     */
    public function isLastPage(): bool
    {
        return $this->lastPage;
    }

    /**
     * @param bool $lastPage
     */
    public function setLastPage(bool $lastPage): void
    {
        $this->lastPage = $lastPage;
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * @param int $currentPage
     */
    public function setCurrentPage(int $currentPage): void
    {
        $this->currentPage = $currentPage;
    }

    /**
     * @return int
     */
    public function getPagesCount(): int
    {
        return $this->pagesCount;
    }

    /**
     * @param int $pagesCount
     */
    public function setPagesCount(int $pagesCount): void
    {
        $this->pagesCount = $pagesCount;
    }
}
