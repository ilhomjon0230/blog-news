<?php

namespace dto;

class PaginationDTO
{
    private int $page;
    private int $size;

    /**
     * @param int $page
     * @param int $size
     */
    public function __construct(int $page = 0, int $size = 9)
    {
        $this->page = $page;
        $this->size = $size;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize(int $size): void
    {
        $this->size = $size;
    }
}
