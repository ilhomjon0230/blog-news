<?php

namespace dto;

class GetBlogsRequest
{
    private PaginationDTO $paginationDTO;
    private BlogsOrder $blogsOrder;
    private int $categoryId;

    /**
     * @param PaginationDTO $paginationDTO
     * @param BlogsOrder $blogsOrder
     * @param int $categoryId
     */
    public function __construct(PaginationDTO $paginationDTO, BlogsOrder $blogsOrder, int $categoryId = -1)
    {
        $this->paginationDTO = $paginationDTO;
        $this->blogsOrder = $blogsOrder;
        $this->categoryId = $categoryId;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId(int $categoryId): void
    {
        $this->categoryId = $categoryId;
    }



    /**
     * @return PaginationDTO
     */
    public function getPaginationDTO(): PaginationDTO
    {
        return $this->paginationDTO;
    }

    /**
     * @param PaginationDTO $paginationDTO
     */
    public function setPaginationDTO(PaginationDTO $paginationDTO): void
    {
        $this->paginationDTO = $paginationDTO;
    }

    /**
     * @return BlogsOrder
     */
    public function getBlogsOrder(): BlogsOrder
    {
        return $this->blogsOrder;
    }

    /**
     * @param BlogsOrder $blogsOrder
     */
    public function setBlogsOrder(BlogsOrder $blogsOrder): void
    {
        $this->blogsOrder = $blogsOrder;
    }
}
