<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Янгиликлар - Блоглар</title>
    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font awesome Css-->
    <link href="assets/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- custom scc -->
    <link href="assets/index.css" rel="stylesheet">
</head>
<body>

<?php
require_once 'config/Autoloader.php';
Autoloader::register();

require_once 'config/config.php';

$active = "home";
include_once "includes/navbar.php";

require_once 'config/Router.php';
$router = new Router();
include_once $router->getCurrentView();

include_once 'includes/footer.php';
?>
</body>

