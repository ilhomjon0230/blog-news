<!-- Footer -->
<footer class="footer mt-5">
    <div class="container">
        <p>Contact:
            <span class="mr-2">Phone: +123456789</span>
            <span class="mr-2">Telegram: @yourtelegram</span>
            <span class="mr-2">Instagram: @yourinstagram</span>
            <span class="mr-2">Facebook: /yourfacebook</span>
        </p>
        <p>&copy; 2023 Your News Website</p>
    </div>
</footer>
