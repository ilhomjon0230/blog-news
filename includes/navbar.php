<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top">
    <a class="navbar-brand" href="/">Янгиликлар-блог сайти</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item
            <?php

            use dto\BlogsOrder;

            if ($active == "home") {
                echo "active";
            }
            ?>">
                <a class="nav-link" href="/">Бош саҳифа</a>
            </li>
            <li class="nav-item
            <?php
            if ($active == "about") {
                echo "active";
            }
            ?>">
                <a class="nav-link" href="/#about">Биз ҳақимизда</a>
            </li>
            <li class="nav-item
            <?php
            if ($active == "category") {
                echo "active";
            }
            ?>">
                <a class="nav-link" href="/#category">Каталог</a>
            </li>

            <?php
            foreach (BlogsOrder::cases() as $blogOrder) {
                echo "
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"/#$blogOrder->name\">$blogOrder->value</a>
                </li>
            ";
            }
            ?>
<!--            <li class="nav-item-->
<!--            --><?php
//            if ($active == "last-news") {
//                echo "active";
//            }
//            ?><!--">-->
<!--                <a class="nav-link" href="/#last-news">Last news</a>-->
<!--            </li>-->
<!--            <li class="nav-item-->
<!--            --><?php
//            if ($active == "trending-news") {
//                echo "active";
//            }
//            ?><!--">-->
<!--                <a class="nav-link" href="/#trending-news">Trending news</a>-->
<!--            </li>-->
<!--            <li class="nav-item-->
<!--            --><?php
//            if ($active == "most-viewed") {
//                echo "active";
//            }
//            ?><!--">-->
<!--                <a class="nav-link" href="/#most-viewed">Popular news</a>-->
<!--            </li>-->
        </ul>
    </div>
</nav>
