<?php
include_once "../includes/navbar.php";
?>

<!-- Page Content -->
<div class="container mt-5">
    <!-- 404 Not Found Page Content -->
    <div class="error-container">
        <h1>404 Not Found</h1>
        <a href="/" class="btn btn-primary mt-3">Back to Home</a>
    </div>
</div>
