<?php
include_once "../includes/navbar.php";
?>

<!-- Page Content -->
<div class="container mt-5">
    <!-- 400 Bad Request Page Content -->
    <div class="error-container">
        <h1>400 Bad request</h1>
        <a href="/" class="btn btn-primary mt-3">Back to Home</a>
    </div>
</div>
