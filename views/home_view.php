<?php

use dao\CategoryRepository;
use dto\BlogsOrder;
use dto\GetBlogsRequest;
use dto\PaginationDTO;
use service\BlogService;
use service\CategoryService;

$blogService = BlogService::instanceOf();
$categoryService = CategoryService::instanceOf();

$categories = $categoryService->getAll();

$blogOrders = BlogsOrder::cases();

?>

<!-- Page Content -->
<div class="container mt-5">
    <!-- Home Page Content -->
    <div id="home">
        <section id="category" class="category">
            <h2>Янгиликлар каталоги</h2>
            <div id="carouselExample" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="/assets/image/news.jpg" class="d-block w-100" alt="Image">
                        <a href="/news">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Барча янгиликлар</h5>
                            </div>
                        </a>
                    </div>
                    <?php
                    foreach ($categories as $category) {
                        $id = $category->getId();
                        $image = $category->getImage();
                        $title = $category->getTitle();
                        echo "
                            <div class=\"carousel-item\">
                                <img src=\"$image\" class=\"d-block w-100\" alt=\"image\">
                                <a href=\"/news?category=$id\">
                                    <div class=\"carousel-caption d-none d-md-block\">
                                        <h5>$title</h5>
                                    </div>
                                </a>
                            </div>
                        ";
                    }
                    ?>
                </div>
                <a class="carousel-control-prev" role="button" href="#carouselExample" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" role="button" href="#carouselExample" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <script>
                function changeCategorySlide(index) {
                    $('#carouselExample').carousel(index);
                }
            </script>
        </section>

        <section id="about" class="about">
            <div class="mt-5">
                <h2>Биз ҳақимизда</h2>
                <h5>
                    <p>
                        Ушбу wебсайт PHP нинг кучини кўрсатиш мақсадида тайёрланди. Ҳозирда wебсайт тоза PHP нинг ўзида
                        ишлаб турибди ҳеч қандай framework ларсиз.
                    </p>
                    <p>
                        - Ушбу wебсайтда каталоглар бўйича филтерлаш имконияти мавжуд
                    </p>
                    <p>
                        - Блог янгиликларни 4 хил кўринишда олиш мумкин:
                        <br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;- Барча янгиликлар
                        <br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;- Сўнги янгиликлар
                        <br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;- Тренддаги янгиликлар, яъни охирги ҳафтада енг кўп кўрилган янгиликлар
                        <br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;- Ва енг кўп кўрилган янгиликлар
                    </p>
                    <p>
                        - Фойдаланувчига қулайлик мақсадида:
                        <br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;- Саҳифалаш функсияси тўлиқ қўшилган
                        <br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;- Шунингдек саҳифанинг ўлчамини ҳам ўзгартитиш мумкин
                    </p>
                    <p>
                        - Ҳозирда wебсайтда 2000 мингга яқин мок блог-янгиликлар қўшилган.
                    </p>
                    <p>
                        - Блог янгиликлардаги расмлар бизга тегишли емас! <a href="https://picsum.photos/">манба</a>
                    </p>
                </h5>
            </div>
        </section>
        <br>
        <?php
        foreach ($blogOrders as $blogOrder) {
            $blogs = $blogService->getBlogsPage(
                new GetBlogsRequest(
                    new PaginationDTO(0, 3),
                    $blogOrder
                ))->getData();

            echo "
            <section id=\"$blogOrder->name\" class=\"$blogOrder->name\">
                <h2>{$blogOrder->value}</h2>
                <div class=\"row\">
            ";
            foreach ($blogs as $blog) {
                $id = $blog->getId();
                $image = $blog->getCardImage();
                $title = $blog->getTitle();
                $createdAt = $blog->getCreatedAt()->format("H:i / d.m.Y");
                $categoryTitle = $blog->getCategoryName();
                echo "
            <div class=\"col-md-4 mb-3\">
                    <a href=\"/news/$id\">
                        <div class=\"card news-card\">
                            <img src=\"$image\" class=\"card-img-top\" alt=\"News Image\">
                            <div class=\"card-body\">
                                <h5 class=\"card-title\">$title</h5>
                                <h6>Каталог: $categoryTitle</h6>
                                <span class=\"date\">
                            <i class=\"fa fa-calendar\" aria-hidden=\"false\"></i>
                                $createdAt
                                </span>
                            </div>
                        </div>
                    </a>
                    </div>
                    ";
            }
            echo "
            </div>
                <div class=\"text-center\">
                    <a href=\"/news?order=$blogOrder->name\" class=\"btn btn-secondary\">Барча $blogOrder->value</a>
                </div>
            </section>
        ";
        }
        ?>
    </div>
    <!-- Bootstrap JS and dependencies -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.2/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.2/dist/js/bootstrap.min.js"></script>
</div>
