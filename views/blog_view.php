<?php

use service\BlogService;

$currentPath = isset($_SERVER['REQUEST_URI']) ? parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) : '/';
$splittedPath = explode("/", $currentPath);
$id = $splittedPath[sizeof($splittedPath) - 1];

$blogService = BlogService::instanceOf();

$blog = $blogService->getById($id);
$blogService->incrementViewsCount($blog->getId());

$createdAt = $blog->getCreatedAt()->format("H:i / d.m.Y");
$viewsCount = $blog->getViewsCount();
$image = $blog->getMainImage();
$title = $blog->getTitle();
$body = $blog->getBody();
?>

<link rel="stylesheet" href="../assets/index.css">
<link href="../assets/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
<!-- Page Content -->
<div class="container mt-5">
    <!-- News Detail Page Content -->
    <div id="news-detail">
        <span class="date m-2">
        <i class="fa fa-calendar" aria-hidden="false"></i>
            <?php echo $createdAt ?>
        </span>
        <span class="views">
            <i class="fa fa-eye" aria-hidden="true"></i>
            <?php echo $viewsCount ?>
        </span>
        <h2 class="m-4">
            <?php echo $title ?>
        </h2>
        <img src="<?php echo $image ?>" class="news-detail-img" alt="News Image">
        <h5 class="mt-4">
            <?php echo $body ?>
        </h5>
    </div>
</div>
