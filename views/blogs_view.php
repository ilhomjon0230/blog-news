<?php

use dto\BlogsOrder;
use dto\GetBlogsRequest;
use dto\PaginationDTO;
use service\BlogService;
use service\CategoryService;

$categoryId = $_GET["category"] ?? -1;
foreach (BlogsOrder::cases() as $case) {
    if (strtolower($case->name) == strtolower($_GET["order"])) {
        $order = $case;
        break;
    }
}
$order = $order ?? BlogsOrder::Last;
$page = $_GET["page"] ?? 0;
$size = $_GET["size"] ?? 9;

$categoryService = CategoryService::instanceOf();
$categories = $categoryService->getAll();

$blogService = BlogService::instanceOf();
$page = $blogService->getBlogsPage(
    new GetBlogsRequest(
        new PaginationDTO($page, $size),
        $order,
        $categoryId
    ));

$blogs = $page->getData();
$isFirstPage = $page->isFirstPage();
$isLastPage = $page->isLastPage();
$currentPage = $page->getCurrentPage();
$pagesCount = $page->getPagesCount();
$visiblePages = 7;
$ellipsis = "...";

$baseURL = "/news?";

// Function to build a URL with additional parameters
function buildURL($page, $pageSize, $category = -1): string
{
    global $baseURL;
    $queryParams = $_GET;
    $queryParams['page'] = $page;
    $queryParams['size'] = $pageSize;
    $queryParams['category'] = $category;
    return $baseURL . http_build_query($queryParams);
}

?>

<!-- Page Content -->
<div class="container mt-5">
    <!-- News Page Content -->
    <div id="news">
        <div class="row">
            <h2>
                <?php echo $order->value; ?>
            </h2>

            <div class="category-selector mb-4 ml-auto">
                <label for="category" class="mr-2 h5"><b>Каталог: </b></label>
                <select id="category" name="category" class="custom-select-sm" onchange="location = this.value;">
                    <?php
                    $selected = ($categoryId == -1) ? 'selected' : '';
                    echo "<option value=\"" . buildURL($currentPage, $size, -1) . "\" $selected>Барча янгиликлар</option>";
                    foreach ($categories as $category) {
                        $selected = ($categoryId == $category->getId()) ? 'selected' : '';
                        echo "<option value=\"" . buildURL($currentPage, $size, $category->getId()) . "\" $selected>{$category->getTitle()}</option>";
                    }
                    ?>
                </select>
            </div>

        </div>

        <div class="row">
            <?php
            foreach ($blogs as $blog) {
                $id = $blog->getId();
                $image = $blog->getCardImage();
                $title = $blog->getTitle();
                $createdAt = $blog->getCreatedAt()->format("H:i / d.m.Y");
                $categoryTitle = $blog->getCategoryName();
                echo "
                    <div class=\"col-md-4 mb-3\">
                    <a href=\"/news/$id\">
                        <div class=\"card news-card\">
                            <img src=\"$image\" class=\"card-img-top\" alt=\"News Image\">
                            <div class=\"card-body\">
                                <h5 class=\"card-title\">$title</h5>
                                <h6>Каталог: $categoryTitle</h6>
                                <span class=\"date\">
                            <i class=\"fa fa-calendar\" aria-hidden=\"false\"></i>
                                $createdAt
                                </span>
                            </div>
                        </div>
                    </a>
                    </div>
                    ";
            }
            ?>
        </div>

        <div class="row d-flex justify-content-center">

            <div class="page-size-selector mb-3 mr-5">
                <label for="page-size" class="mr-2">Page Size:</label>
                <select id="page-size" name="page_size" class="custom-select-sm" onchange="location = this.value;">
                    <?php
                    $availablePageSizes = [6, 9, 15, 24, 36]; // You can customize the available page sizes
                    foreach ($availablePageSizes as $pageSize) {
                        $selected = ($size == $pageSize) ? 'selected' : '';
                        echo "<option value=\"" . buildURL(0, $pageSize, $categoryId) . "\" $selected>$pageSize</option>";
                    }
                    ?>
                </select>
            </div>
            <nav aria-label="Page navigation">
                <ul class="pagination justify-content-center">
                    <!-- Previous button -->
                    <li class="page-item <?php echo $isFirstPage ? "disabled" : "" ?>">
                        <a class="page-link" href="<?php echo buildURL($currentPage - 1, $size, $categoryId); ?>"
                           tabindex="-1" aria-disabled="true">
                            Previous
                        </a>
                    </li>

                    <?php
                    $startIndex = max(1, $currentPage - floor($visiblePages / 2));
                    $endIndex = min($startIndex + $visiblePages - 1, $pagesCount);

                    if ($startIndex > 1) {
                        echo "
                        <li class=\"page-item\">
                            <a class=\"page-link\" href=\"" . buildURL(1, $size, $categoryId) . "\">1</a>
                        </li>
                        <li class=\"page-item disabled\">
                            <span class=\"page-link\">$ellipsis</span>
                        </li>
                        ";
                    }

                    for ($i = $startIndex; $i <= $endIndex; $i++) {
                        echo "
                        <li class=\"page-item " . ($currentPage == $i - 1 ? "active" : "") . "\">
                            <a class=\"page-link\" href=\"" . buildURL($i - 1, $size, $categoryId) . "\">$i</a>
                        </li>
                        ";
                    }

                    if ($endIndex < $pagesCount) {
                        echo "
                        <li class=\"page-item disabled\">
                            <span class=\"page-link\">$ellipsis</span>
                        </li>
                        <li class=\"page-item\">
                            <a class=\"page-link\" href=\"" . buildURL($pagesCount - 1, $size, $categoryId) . "\">$pagesCount</a>
                        </li>
                    ";
                    }
                    ?>
                    <!-- Next button -->
                    <li class="page-item <?php echo $isLastPage ? "disabled" : "" ?>">
                        <a class="page-link"
                           href="<?php echo buildURL($currentPage + 1, $size, $categoryId); ?>">Next</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
