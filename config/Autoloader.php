<?php

class Autoloader
{
    public static function register()
    {
        spl_autoload_register([__CLASS__, 'autoload']);
    }

    private static function autoload($className)
    {
        $classPath = str_replace('\\', DIRECTORY_SEPARATOR, $className) . '.php';

        if (file_exists($classPath)) {
            include_once $classPath;
        }
    }
}
