<?php

class Router
{
    private $paths;

    public function __construct()
    {
        $this->paths = array(
            "/" => "home_view",
            "/news" => "blogs_view",
            "/news/*" => "blog_view",
            "/404" => "view_404",
            "/400" => "view_400"
        );
    }

    public function addPath($uri, $view_file) {
        $this->paths[$uri] = $view_file;
    }

    public function getCurrentView() {
        $currentPath = isset($_SERVER['REQUEST_URI']) ? parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) : '/';

        foreach ($this->paths as $path => $view) {
            if ($this->matchesPath($currentPath, $path)) {
                return "views/" . $view . ".php";
            }
        }
        // If no match is found, you can return a default view or handle it accordingly
        return "views/view_404.php";
    }

    private function matchesPath($currentPath, $path) {
        // Handle wildcard (*) in path
        if (strpos($path, '*') !== false) {
            $pattern = str_replace('*', '.*', $path);
            return (bool) preg_match("#^{$pattern}$#", $currentPath);
        }
        // Exact match
        return $currentPath === $path;
    }

    /**
     * @return string[]
     */
    public function getPaths()
    {
        return $this->paths;
    }
}


?>