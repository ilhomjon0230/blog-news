<?php

namespace service;

use dao\CategoryRepository;

class CategoryService
{
    private CategoryRepository $categoryRepository;
    private static CategoryService $categoryService;

    private function __construct()
    {
        $this->categoryRepository = CategoryRepository::instanceOf();
    }

    public static function instanceOf(): CategoryService
    {
        if (isset(self::$categoryService)) {
            return self::$categoryService;
        }
        self::$categoryService = new CategoryService();
        return self::$categoryService;
    }

    public function getAll(): array
    {
        return $this->categoryRepository->selectAll();
    }
}