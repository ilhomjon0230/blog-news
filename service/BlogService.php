<?php

namespace service;

use dao\BlogRepository;
use dto\BlogsOrder;
use dto\GetBlogsRequest;
use dto\Page;
use model\Blog;

class BlogService
{
    private BlogRepository $blogRepository;
    private static BlogService $blogService;

    private function __construct()
    {
        $this->blogRepository = BlogRepository::instanceOf();
    }

    public static function instanceOf(): BlogService
    {
        if (isset(self::$blogService)) {
            return self::$blogService;
        }
        self::$blogService = new BlogService();
        return self::$blogService;
    }


    public function getById(int $id): Blog
    {
        $blog = $this->blogRepository->selectById($id);
        if (!$blog) {
            header('Location: /404');
            exit();
        }
        return $blog;
    }

    public function incrementViewsCount(int $id): void
    {
        $blog = $this::getById($id);
        $blog->setViewsCount($blog->getViewsCount() + 1);
        $this->blogRepository->update($blog);
    }

    public function getBlogsPage(GetBlogsRequest $getBlogsRequest): Page
    {
        $blogsCount = $this->blogRepository->selectCount();
        $pagesCount = ceil($blogsCount / $getBlogsRequest->getPaginationDTO()->getSize());
        if ($getBlogsRequest->getPaginationDTO()->getPage() < 0 ||
            $getBlogsRequest->getPaginationDTO()->getPage() > $pagesCount - 1 ||
            $getBlogsRequest->getPaginationDTO()->getSize() > 100) {
            header('Location: /400');
            exit();
        }
        $data = [];
        if ($getBlogsRequest->getCategoryId() == -1) {
            switch ($getBlogsRequest->getBlogsOrder()) {
                case BlogsOrder::Last:
                    $data = $this->blogRepository->selectLastAddedPage($getBlogsRequest->getPaginationDTO());
                    break;
                case BlogsOrder::MostViewed:
                    $data = $this->blogRepository->selectMostViewedPage($getBlogsRequest->getPaginationDTO());
                    break;
                case BlogsOrder::Trending:
                    $data = $this->blogRepository->selectMostViewedInWeekPage($getBlogsRequest->getPaginationDTO());
                    break;
            }
        } else {
            switch ($getBlogsRequest->getBlogsOrder()) {
                case BlogsOrder::Last:
                    $data = $this->blogRepository->selectLastAddedPageWithCategory(
                        $getBlogsRequest->getPaginationDTO(),
                        $getBlogsRequest->getCategoryId()
                    );
                    break;
                case BlogsOrder::MostViewed:
                    $data = $this->blogRepository->selectMostViewedPageWithCategory(
                        $getBlogsRequest->getPaginationDTO(),
                        $getBlogsRequest->getCategoryId()
                    );
                    break;
                case BlogsOrder::Trending:
                    $data = $this->blogRepository->selectMostViewedInWeekPageWithCategory(
                        $getBlogsRequest->getPaginationDTO(),
                        $getBlogsRequest->getCategoryId()
                    );
                    break;
            }
        }
        return new Page(
            $data,
            $getBlogsRequest->getPaginationDTO()->getPage() == 0,
            $getBlogsRequest->getPaginationDTO()->getPage() == $pagesCount - 1,
            $getBlogsRequest->getPaginationDTO()->getPage(),
            $pagesCount
        );
    }
}
