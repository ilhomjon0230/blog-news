<?php

namespace model;

abstract class BaseEntity
{
    private int $id;
    private \DateTime $createdAt;
    private \DateTime $updatedAt;

    /**
     * @param int $id
     * @param \DateTime $createdAt
     * @param \DateTime $updatedAt
     */
    public function __construct(int $id, \DateTime $createdAt, \DateTime $updatedAt)
    {
        $this->id = $id;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return BaseEntity
     */
    public function setId(int $id): BaseEntity
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return BaseEntity
     */
    public function setCreatedAt(\DateTime $createdAt): BaseEntity
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return BaseEntity
     */
    public function setUpdatedAt(\DateTime $updatedAt): BaseEntity
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
