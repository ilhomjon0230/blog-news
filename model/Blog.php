<?php

namespace model;

use DateTime;

class Blog extends BaseEntity
{
    private string $mainImage;
    private string $cardImage;
    private string $title;
    private string $body;
    private int $viewsCount;
    private int $categoryId;
    private string $categoryName;

    /**
     * @param int $categoryId
     * @param string $mainImage
     * @param string $cardImage
     * @param string $title
     * @param string $body
     * @param int $viewsCount
     * @param int|null $id
     * @param DateTime|null $createdAt
     * @param DateTime|null $updatedAt
     */
    public function __construct(
        int      $categoryId,
        string   $categoryName = "",
        string   $mainImage,
        string   $cardImage,
        string   $title,
        string   $body,
        int      $viewsCount = 0,
        int      $id = null,
        DateTime $createdAt = null,
        DateTime $updatedAt = null,
    )
    {
        parent::__construct($id, $createdAt, $updatedAt);
        $this->categoryId = $categoryId;
        $this->categoryName = $categoryName;
        $this->mainImage = $mainImage;
        $this->cardImage = $cardImage;
        $this->title = $title;
        $this->body = $body;
        $this->viewsCount = $viewsCount;
    }

    /**
     * @return string
     */
    public function getCategoryName(): string
    {
        return $this->categoryName;
    }

    /**
     * @param string $categoryName
     */
    public function setCategoryName(string $categoryName): void
    {
        $this->categoryName = $categoryName;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId(int $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return string
     */
    public function getMainImage(): string
    {
        return $this->mainImage;
    }

    /**
     * @param string $mainImage
     * @return Blog
     */
    public function setMainImage(string $mainImage): Blog
    {
        $this->mainImage = $mainImage;
        return $this;
    }

    /**
     * @return string
     */
    public function getCardImage(): string
    {
        return $this->cardImage;
    }

    /**
     * @param string $cardImage
     * @return Blog
     */
    public function setCardImage(string $cardImage): Blog
    {
        $this->cardImage = $cardImage;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Blog
     */
    public function setTitle(string $title): Blog
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param string $body
     * @return Blog
     */
    public function setBody(string $body): Blog
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return int
     */
    public function getViewsCount(): int
    {
        return $this->viewsCount;
    }

    /**
     * @param int $viewsCount
     * @return Blog
     */
    public function setViewsCount(int $viewsCount): Blog
    {
        $this->viewsCount = $viewsCount;
        return $this;
    }
}