<?php

namespace model;

use DateTime;

class Category extends BaseEntity
{
    private string $image;
    private string $title;

    /**
     * @param string $image
     * @param string $title
     * @param int|null $id
     * @param DateTime|null $createdAt
     * @param DateTime|null $updatedAt
     */
    public function __construct(
        string   $image,
        string   $title,
        int      $id = null,
        DateTime $createdAt = null,
        DateTime $updatedAt = null,
    )
    {
        parent::__construct($id, $createdAt, $updatedAt);
        $this->image = $image;
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return Category
     */
    public function setImage(string $image): Category
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Category
     */
    public function setTitle(string $title): Category
    {
        $this->title = $title;
        return $this;
    }
}
