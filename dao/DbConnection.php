<?php

namespace dao;

use PgSql\Connection;

class DbConnection
{
    private string $host;
    private string $port;
    private string $database;
    private string $username;
    private string $password;
    private Connection $connection;
    private static DbConnection $dbConnection;
    private function __construct()
    {
        $this->host = getenv("db_host");
        $this->port = getenv("db_port");
        $this->database = getenv("db_name");
        $this->username = getenv("db_username");
        $this->password = getenv("db_password");
        if (!isset($this->connection)) {
            $this->connection = \pg_connect(
                "host=$this->host port=$this->port dbname=$this->database user=$this->username password=$this->password"
            );
        }
    }

    public static function instanceOf(): DbConnection {
        if (isset(self::$dbConnection)) {
            return self::$dbConnection;
        }
        self::$dbConnection = new DbConnection();
        return self::$dbConnection;
    }

    public function getConnection(): Connection
    {
        return $this->connection;
    }

//    public function __destruct()
//    {
//        pg_close($this->connection);
//    }
}
