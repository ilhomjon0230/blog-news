<?php

namespace dao;

use model\Category;
use PgSql\Connection;
use PgSql\Result;

class CategoryRepository
{
    private Connection $connection;
    private string $tableName;
    private static CategoryRepository $categoryRepository;

    private function __construct()
    {
        $this->connection = DbConnection::instanceOf()->getConnection();
        $this->tableName = "blog_category"; // Assuming the table name is "categories"
//        $this->createTable();
    }

    public static function instanceOf(): CategoryRepository
    {
        if (isset(self::$categoryRepository)) {
            return self::$categoryRepository;
        }
        self::$categoryRepository = new CategoryRepository();
        return self::$categoryRepository;
    }

//    private function createTable(): void
//    {
//        $query = "
//        CREATE TABLE IF NOT EXISTS $this->tableName (
//            id SERIAL PRIMARY KEY,
//            image VARCHAR,
//            title VARCHAR(100),
//            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
//            updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
//        );
//        ";
//
//        $this->sendQuery($query);
//    }

    public function add(Category $category): void
    {
        $query = "
        INSERT INTO $this->tableName (image, title)
        VALUES ('{$category->getImage()}', '{$category->getTitle()}');
        ";

        $this->sendQuery($query);
    }

    public function update(Category $category): void
    {
        $query = "
        UPDATE $this->tableName
        SET image = '{$category->getImage()}',
            title = '{$category->getTitle()}',
            updated_at = NOW()
        WHERE id = {$category->getId()};
        ";

        $this->sendQuery($query);
    }

    public function deleteById(int $id): void
    {
        $query = "DELETE FROM $this->tableName WHERE id = $id;";
        $this->sendQuery($query);
    }

    public function selectAll(): array
    {
        $query = "SELECT * FROM $this->tableName;";
        $result = $this->sendQuery($query);

        $categories = [];
        while ($categoryData = pg_fetch_assoc($result)) {
            $category = $this->createCategoryFromData($categoryData);
            $categories[] = $category;
        }

        return $categories;
    }

    public function selectById(int $id): ?Category
    {
        $query = "SELECT * FROM $this->tableName WHERE id = $id;";
        $result = $this->sendQuery($query);

        $categoryData = pg_fetch_assoc($result);

        if (!$categoryData) {
            return null;
        }

        return $this->createCategoryFromData($categoryData);
    }

    private function createCategoryFromData(array $categoryData): Category
    {
        $category = new Category(
            $categoryData['image'],
            $categoryData['title'],
            $categoryData['id'],
            new \DateTime($categoryData['created_at']),
            new \DateTime($categoryData['updated_at'])
        );

        return $category;
    }

    private function sendQuery(string $query): Result|bool
    {
        return pg_query($this->connection, $query);
    }
}
