<?php

namespace dao;

use DateTime;
use dto\PaginationDTO;
use model\Blog;
use PgSql\Connection;

class BlogRepository
{
    private Connection $connection;
    private CategoryRepository $categoryRepository;
    private string $tableName;

    private static BlogRepository $blogRepository;

    private function __construct()
    {
        $this->connection = DbConnection::instanceOf()->getConnection();
        $this->categoryRepository = CategoryRepository::instanceOf();
        $this->tableName = "blog";
//        $this->createTable();
    }

    public static function instanceOf(): BlogRepository
    {
        if (isset(self::$blogRepository)) {
            return self::$blogRepository;
        }
        self::$blogRepository = new BlogRepository();
        return self::$blogRepository;
    }

//    private function createTable(): void
//    {
//        $query = "
//        CREATE TABLE IF NOT EXISTS $this->tableName (
//            id SERIAL PRIMARY KEY NOT NULL,
//            main_image VARCHAR NOT NULL,
//            card_image VARCHAR NOT NULL,
//            body TEXT NOT NULL,
//            created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
//            updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
//            views_count INT NOT NULL
//        );";
//        $this->sendQuery($query);
//    }

    public function add(): bool
    {
        // TODO
        return false;
    }

    public function deleteById(): bool
    {
        // TODO
        return false;
    }

    public function selectById(string $id): ?Blog
    {
        $query = "
        SELECT * 
        FROM $this->tableName
        WHERE id = $id
        ";

        $result = $this->sendQuery($query);
        $blogData = pg_fetch_assoc($result);
        if (!$blogData) {
            return null;
        }

        return $this->createBlogFromData($blogData);
    }

    public function selectCount(): int
    {
        $query = "
        SELECT count(*) 
        FROM $this->tableName
        ";

        $result = $this->sendQuery($query);
        return (int) pg_fetch_assoc($result)["count"];
    }

    public function selectLastAddedPage(PaginationDTO $pagination): array
    {
        $query = "
        SELECT *
        FROM $this->tableName
        ORDER BY created_at DESC
        LIMIT {$pagination->getSize()} OFFSET {$pagination->getPage()};
    ";

        $result = $this->sendQuery($query);
        $blogs = [];

        while ($blogData = pg_fetch_assoc($result)) {
            $blog = $this->createBlogFromData($blogData);
            $blogs[] = $blog;
        }

        return $blogs;
    }

    public function selectMostViewedPage(PaginationDTO $pagination): array
    {
        $query = "
        SELECT *
        FROM $this->tableName
        ORDER BY views_count DESC
        LIMIT {$pagination->getSize()} OFFSET {$pagination->getPage()};
    ";

        $result = $this->sendQuery($query);
        $blogs = [];

        while ($blogData = pg_fetch_assoc($result)) {
            $blog = $this->createBlogFromData($blogData);
            $blogs[] = $blog;
        }

        return $blogs;
    }

    public function selectMostViewedInWeekPage(PaginationDTO $pagination): array
    {
        $query = "
        SELECT *
        FROM $this->tableName
        WHERE created_at >= (NOW() - '1 WEEK'::INTERVAL)
        ORDER BY views_count DESC
        LIMIT {$pagination->getSize()} OFFSET {$pagination->getPage()};
        ";

        $result = $this->sendQuery($query);
        $blogs = [];

        while ($blogData = pg_fetch_assoc($result)) {
            $blog = $this->createBlogFromData($blogData);
            $blogs[] = $blog;
        }

        return $blogs;
    }

    public function selectLastAddedPageWithCategory(PaginationDTO $pagination, int $categoryId): array
    {
        $query = "
        SELECT *
        FROM $this->tableName
        WHERE category_id = $categoryId
        ORDER BY created_at DESC
        LIMIT {$pagination->getSize()} OFFSET {$pagination->getPage()};
    ";

        $result = $this->sendQuery($query);
        $blogs = [];

        while ($blogData = pg_fetch_assoc($result)) {
            $blog = $this->createBlogFromData($blogData);
            $blogs[] = $blog;
        }
        return $blogs;
    }

    public function selectMostViewedPageWithCategory(PaginationDTO $pagination, int $categoryId): array
    {
        $query = "
        SELECT *
        FROM $this->tableName
        WHERE category_id = $categoryId
        ORDER BY views_count DESC
        LIMIT {$pagination->getSize()} OFFSET {$pagination->getPage()};
    ";

        $result = $this->sendQuery($query);
        $blogs = [];

        while ($blogData = pg_fetch_assoc($result)) {
            $blog = $this->createBlogFromData($blogData);
            $blogs[] = $blog;
        }

        return $blogs;
    }

    public function selectMostViewedInWeekPageWithCategory(PaginationDTO $pagination, int $categoryId): array
    {
        $query = "
        SELECT *
        FROM $this->tableName
        WHERE created_at >= (NOW() - '1 WEEK'::INTERVAL) AND category_id = $categoryId
        ORDER BY views_count DESC
        LIMIT {$pagination->getSize()} OFFSET {$pagination->getPage()};
        ";

        $result = $this->sendQuery($query);
        $blogs = [];

        while ($blogData = pg_fetch_assoc($result)) {
            $blog = $this->createBlogFromData($blogData);
            $blogs[] = $blog;
        }

        return $blogs;
    }

    public function update(Blog $blog): void
    {
        $query = "
        UPDATE $this->tableName
        SET main_image = '{$blog->getMainImage()}',
            card_image = '{$blog->getCardImage()}',
            title = '{$blog->getTitle()}',
            body = '{$blog->getBody()}',
            views_count = {$blog->getViewsCount()},
            category_id = {$blog->getCategoryId()}
        WHERE id = {$blog->getId()}
        ";
        $this->sendQuery($query);
    }

    private function sendQuery(string $query)
    {
        return pg_query($this->connection, $query);
    }

    private function createBlogFromData(array $blogData): Blog
    {
        $categoryId = $blogData['category_id'];
        $category = $this->categoryRepository->selectById($categoryId);
        return new Blog(
            categoryId: $categoryId,
            categoryName: $category->getTitle(),
            mainImage: $blogData['main_image'],
            cardImage: $blogData['card_image'],
            title: $blogData['title'],
            body: $blogData['body'],
            viewsCount: $blogData['views_count'],
            id: $blogData['id'],
            createdAt: new DateTime($blogData['created_at']),
            updatedAt: new DateTime($blogData['updated_at']),
        );
    }
}
